class CuadriculaEnemigos {
    constructor() {
        this.position = {
            x: 0,
            y: 0
        }

        this.speed = {
            x: 5,
            y: 0
        }
        this.enemigos = [];

        //Nos da un valor random entre 2 y 7 para las filas
        const rows = Math.floor(Math.random() * 7 + 2);
        const columns = Math.floor(Math.random() * 5 + 2);

        this.width = rows * 40;
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < columns ; j++)
                this.enemigos.push(new Enemigo({
                    posicion: {
                        x: i * 40,
                        y: j * 50
                    }
                }));
        }

    }

    update() {
        this.position.x += this.speed.x;
        this.position.y += this.speed.y;

        this.speed.y = 0;

        if (this.position.x + this.width >= canvas.width || this.position.x <= 0) {
            this.speed.x = -this.speed.x;
            //Por cada frame que pasa en el que se mueve de un lado para otro
            //los enemigos van a bajar 30 píxeles
            this.speed.y = 30;
        }
    }



}