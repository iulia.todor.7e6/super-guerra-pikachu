class Enemigo {
    constructor({posicion}) {

        this.speed = {
            x: 0,
            y: 0
        }

        const image = new Image();
        //La imagen que representa al jugador
        var imagenes = ['images/lucario.png', 'images/incineroar.png', 'images/lopunny.png', 'images/cinderace.png', 'images/meowscarada.png']
        let randomSprite = Math.floor((Math.random() * 5))

        image.src = imagenes[randomSprite];
        //Para que todo cargue cuando la imagen haya terminado de cargar
        image.onload = () => {
            //Esta variable representa el aumento o disminución de tamaño de la imagen
            const scale = 1.75;
            this.sprite = image;
            //Definimos el tamaño de la imagen usando su tamaño base multiplicado por la escala
            this.width = image.width * scale;
            this.height = image.height * scale;
            this.position = {
                //Definimos la posición inicial del jugador para que esté abajo en el centro
                x: posicion.x,
                y: posicion.y
            }
        }
    }

    //Dibuja el sprite del jugador
    draw() {
        //c.fillStyle = 'red'
        //c.fillRect(this.position.x, this.position.y, this.width, this.height)
        c.drawImage(this.sprite, this.position.x, this.position.y, this.width, this.height);

    }

    update({speed}) {
        //El condicional es para que solo dibuje la imagen si esta ha cargado
        if (this.sprite) {
            this.draw();
            this.position.x += speed.x;
            this.position.y += speed.y;
        }
    }
}


