class Proyectil {
    constructor({posicion, speed}){
        this.posicion = posicion
        this.speed = speed
    
        const image = new Image();
        //La imagen que representa al jugador
        image.src = 'images/proyectilPikachu.png';
        //Para que todo cargue cuando la imagen haya terminado de cargar
            //Esta variable representa el aumento o disminución de tamaño de la imagen
            const scale = 0.5;
            this.sprite = image;
            //Definimos el tamaño de la imagen usando su tamaño base multiplicado por la escala
            this.width = image.width * scale;
            this.height = image.height * scale;
            this.position = {
                //Definimos la posición inicial del jugador para que esté abajo en el centro
                x: posicion.x,
                y: posicion.y
            }
            this.speed = {
                x: speed.x,
                y: speed.y
            }
    }

    draw() {
        c.drawImage(this.sprite, this.posicion.x, this.posicion.y, this.width, this.height);
    }

    update() {
        if(this.sprite) {
        this.draw()
        this.posicion.x += this.speed.x;
        this.posicion.y += this.speed.y;
        }
    }

}