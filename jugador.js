class Jugador {
    constructor() {

        this.speed = {
            x: 0,
            y: 0
        }

        const image = new Image();
        //La imagen que representa al jugador
        image.src = 'images/pikachu-back.png';
        //Para que todo cargue cuando la imagen haya terminado de cargar
        image.onload = () => {
            //Esta variable representa el aumento o disminución de tamaño de la imagen
            const scale = 1.5;
            this.sprite = image;
            //Definimos el tamaño de la imagen usando su tamaño base multiplicado por la escala
            this.width = image.width * scale;
            this.height = image.height * scale;
            this.position = {
                //Definimos la posición inicial del jugador para que esté abajo en el centro
                x: canvas.width / 2 - this.width / 2,
                y: canvas.height - this.height - 20
            }
        }
    }

    //Dibuja el sprite del jugador
    draw() {
        //c.fillStyle = 'red'
        //c.fillRect(this.position.x, this.position.y, this.width, this.height)
        c.drawImage(this.sprite, this.position.x, this.position.y, this.width, this.height);

    }

    update() {
        //El condicional es para que solo dibuje la imagen si esta ha cargado
        if (this.sprite) {
            this.draw();
            this.position.x += this.speed.x;

            if(this.position.x + this.sprite.width >= canvas.width) {
                this.position.x = canvas.width - this.sprite.width;
            }
            else if(this.position.y + this.sprite.height <= canvas.height-canvas.height) {
                this.position.y = canvas.height - this.sprite.height;
            }
        }
    }
}


