const canvas = document.querySelector('canvas');
const c = canvas.getContext('2d');

//Toma todo el tamaño de la ventana
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const jugador = new Jugador();
const cuadriculas = [];
const proyectiles = [new Proyectil({
    posicion: {
        x: 300,
        y: 300
    },
    speed: {
        x: 5,
        y: 0
    }
})];

let frames = 0;
let randomInterval = Math.floor((Math.random() * 500) + 500);


//Carga la imagen de forma constante
function animate() {
    window.requestAnimationFrame(animate);
    //fondo
    c.fillStyle = 'black';
    c.fillRect(0, 0, canvas.width, canvas.height);
    jugador.update();

    proyectiles.forEach((proyectil, index) => {
        if (proyectil.posicion.y <= 0) {
            setTimeout(() => {
                proyectiles.splice(index, 1);
            }, 0) //Queremos un frame aditional antes de que splice actúe

        }
        else {
             proyectil.update();
        }
       
    })

    cuadriculas.forEach(cuadricula => {
        cuadricula.update();
        cuadricula.enemigos.forEach((enemigo, i) => {
            enemigo.update({ speed: cuadricula.speed });

        })
    })
    //Crea una nueva cuadrícula cada 1000 frames
    if (frames % randomInterval === 0) {
        cuadriculas.push(new CuadriculaEnemigos());
        //El valor cambia cada vez que aparecen enemigos
        randomInterval = Math.floor((Math.random() * 500) + 500);
        frames = 0;
    }
    frames++;
}

animate();

window.addEventListener('mousemove', (function (e) {
    var posicionX = e.clientX;
    var posicionY = e.clientY;
    console.log(canvas.width);
    jugador.position.x = posicionX;
    jugador.position.y = posicionY;
    /****/
}))


//Sacamos la propiedad key con los corchetes sin tener que referencia a su padre (event) directamente
window.addEventListener('keydown', ({ key }) => {
    switch (key) {
        case 'a':
            console.log('attack');
            proyectiles.push(new Proyectil({
                posicion: {
                    x: jugador.position.x + jugador.width / 2,
                    y: jugador.position.y
                },
                speed: {
                    x: 0,
                    y: -10
                }
            }))
            break;
    }
})




